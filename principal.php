<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Runarcana</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />

    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>

<?php include 'inc/navbar.php' ?>


<div class="container">
  <div class="row justify-content-md-center">
   
  <?php include 'inc/menu.php' ?>

    <div class="col-9">

        <h3>Bem vindo à Plataforma do RPG Runarcana</h3>


        <div class="status">
            <h3>Sua última sessão: <span style="color:blue"> Nome da Campanha </span> </h3> <br>
            <h3>Jogadores:</h3>
            <br>
            <h4>Naegii <span style="color:red">(Constructo 6)</span> <span style="color:brown">Status:Vivo</span></h4><br>
            <h4>Fern <span style="color:red">(Yordle Maga 6)</span> <span style="color:brown">Status:Vivo</span></h4><br>
            <h4>Aqua <span style="color:red">(Lothan Ladino 6)</span> <span style="color:brown">Status:Morto</span></h4><br>
            <h4>Claimh <span style="color:red">(Humano Artificier 6)</span> <span style="color:brown">Status:Vivo</span></h4><br>

        </div>
   

    </div>

  </div>
</div>

</body>
</html>