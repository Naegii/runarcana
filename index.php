<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Runarcana</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />

    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
<div class="container">
  <div class="row justify-content-md-center">
   
    <div class="col-md-auto">
      <h2>Bem vindo à plataforma de Runarcana</h2>
    </div>

  </div>
</div>
<div class="container">
  <div class="row justify-content-md-center">
   
    <div class="col-md-auto">
      <div class="form-login">

        <h3>Realize seu Login para acessar a plataforma</h3>
        <label for="login">Login/E-mail</label> <input class="form_control" type="text" name="login"><br>
        <label for="login">Senha</label> <input class="form_control" type="password" name="password"><br>
        <div class="form_submit" type="submit">Entrar</div>
      </div>

      <a href="#">Esqueci a senha</a><br>
      <a href="#">Criar uma conta</a>
    </div>

  </div>
</div>

</body>
</html>