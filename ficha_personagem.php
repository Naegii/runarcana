<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Runarcana</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />

    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>

<?php include 'inc/navbar.php' ?>


<div class="container">
  <div class="row justify-content-md-center">
   
  <?php include 'inc/menu.php' ?>

    <div class="col-9">

        <h3>Naegii</h3>

    <div class="caracteristicas">
    <span>Origem:<span> <input type="text" name="origem"> <br>
    <span>Região:<span> <input type="text" name="regiao"> <br>
    <span>Passado:<span> <input type="text" name="passado"> <br>
    <span>Alinhamento:<span> <input type="text" name="alinhamento"> <br>
    <span>Classe E Nível:<span> <input type="text" name="classe"> <br>

    <span>Aparência</span><br>
    <span>Gênero:<span> <input type="text" name="genero"> <br>
    <span>Idade:<span> <input type="text" name="idade"> <br>
    <span>Altura & Peso:<span> <input type="text" name="altura"> <br>
    <span>Tom de Pele:<span> <input type="text" name="pele"> <br>
    <span>Olhos:<span> <input type="text" name="olhos"> <br>
    <span>Cabelos:<span> <input type="text" name="cabelo"> <br>
    <span>Biografia</span><br>
    <textarea name="biografia"></textarea>

    </div>  

    <div class="pericias">
        <input type="checkbox" name="acrobacia"><input type="number" name="acro_mod"><span>Acrobacia (DES)</span><br>
        <input type="checkbox" name="arcanismo"><input type="number" name="arca_mod"><span>Arcanismo (INT)</span><br>
        <input type="checkbox" name="atletismo"><input type="number" name="atle_mod"><span>Atletismo (FOR)</span><br>
        <input type="checkbox" name="atuacao"><input type="number" name="atua_mod"><span>Atuação (CAR)</span><br>
        <input type="checkbox" name="enganacao"><input type="number" name="enga_mod"><span>Enganação (CAR)</span><br>
        <input type="checkbox" name="furtividade"><input type="number" name="furt_mod"><span>Furtividade (DES)</span><br>
        <input type="checkbox" name="historia"><input type="number" name="hist_mod"><span>História (INT)</span><br>
        <input type="checkbox" name="intimidacao"><input type="number" name="intim_mod"><span>Intimidação (CAR)</span><br>
        <input type="checkbox" name="intuicao"><input type="number" name="intui_mod"><span>Intuição (SAB)</span><br>
        <input type="checkbox" name="investigacao"><input type="number" name="inves_mod"><span>Investigação (INT)</span><br>
        <input type="checkbox" name="animais"><input type="number" name="animais_mod"><span>Lidar com Animais(SAB)</span><br>
        <input type="checkbox" name="medicina"><input type="number" name="med_mod"><span>Medicina (SAB)</span><br>
        <input type="checkbox" name="natureza"><input type="number" name="nat_mod"><span>Natureza (INT)</span><br>
        <input type="checkbox" name="percepcao"><input type="number" name="perc_mod"><span>Percepção (SAB)</span><br>
        <input type="checkbox" name="persuasao"><input type="number" name="pers_mod"><span>Persuasão (CAR)</span><br>
        <input type="checkbox" name="prestidigitacao"><input type="number" name="pres_mod"><span>Prestidigitação (DES)</span><br>
        <input type="checkbox" name="religiao"><input type="number" name="rel_mod"><span>Religião (INT)</span><br>
        <input type="checkbox" name="sobrevivencia"><input type="number" name="sobr_mod"><span>Sobrevivência (SAB)</span><br>
        <input type="checkbox" name="tecnologia"><input type="number" name="tec_mod"><span>Tecnologia (INT)</span><br>
    </div>

    <div class="status">
        <span>Força<span><br>
    <input type="number" name="mod_for" value="6"><input type="number" name="for" value="10"><br>

    <span>Destreza<span><br>
    <input type="number" name="mod_des" value="6"><input type="number" name="des" value="10"><br>

    <span>Constituição<span><br>
    <input type="number" name="mod_con" value="6"><input type="number" name="con" value="10"><br>

    <span>Inteligência<span><br>
    <input type="number" name="mod_int" value="6"><input type="number" name="int" value="10"><br>

    <span>Sabedoria<span><br>
    <input type="number" name="mod_sab" value="6"><input type="number" name="sab" value="10"><br>

    <span>Carisma<span><br>
    <input type="number" name="mod_car" value="6"><input type="number" name="car" value="10"><br>
    </div>

  </div>


</div>

</body>
</html>
