<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Runarcana</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />

    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>

<?php include 'inc/navbar.php' ?>


<div class="container">
  <div class="row justify-content-md-center">
   
  <?php include 'inc/menu.php' ?>

    <div class="col-9">

        <h3>Personagens</h3>


        <div class="status">
            <h3>Sua última sessão: <span style="color:blue"> Gélido Lamento </span> </h3> <br>
            <br>

            <h4><a href="/ficha_personagem">Naegii</a><span style="color:red">(Constructo 6)</span> Campanha:<span style="color:brown">Gélido Lamento</span></h4><br>
            <h4><a href="/ficha_personagem">Naenarius </a><span style="color:red">(Yordle Maga 6)</span> Campanha:<span style="color:brown">Pico do Lamento</span></h4><br>
            <h4><a href="/ficha_personagem">Naenius </a><span style="color:red">(Lothan Ladino 6)</span> Campanha:<span style="color:brown">Stravarius</span></h4><br>
            
            <div>
                <a href="/ficha_personagem"> <span>Criar novo personagem </span> </a>
        </div>
   

    </div>

  </div>
</div>

</body>
</html>
